/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {StyleSheet, useColorScheme, View, Button} from 'react-native';

import {launchImageLibrary, launchCamera} from 'react-native-image-picker';

import {Colors} from 'react-native/Libraries/NewAppScreen';

const App = () => {
  const isDarkMode = useColorScheme() === 'dark';

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };

  const handleTaskPhoto = () => {
    console.log('handleTaskPhoto');
    let options = {};
    launchCamera(options, response => {
      console.log('response', response);
      try {
        console.log('compleat');
        if (response.didCancel) {
          console.log('User cancelled image picker');
        } else if (response.error) {
          console.log('ImagePicker Error: ', response.error);
        } else if (response.customButton) {
          console.log('User tapped custom button: ', response.customButton);
          alert(response.customButton);
        } else {
          const source = {uri: response.uri};
          console.log('response', JSON.stringify(response));
          this.setState({
            filePath: response,
            fileData: response.data,
            fileUri: response.uri,
          });
        }
      } catch (error) {
        console.log(error);
      }
    });
  };

  const handleGallery = () => {
    console.log('handleTaskPhoto');
    let options = {};
    launchImageLibrary(options, response => {
      console.log('response', response);
      try {
        console.log('compleat');
        if (response.didCancel) {
          console.log('User cancelled image picker');
        } else if (response.error) {
          console.log('ImagePicker Error: ', response.error);
        } else if (response.customButton) {
          console.log('User tapped custom button: ', response.customButton);
          alert(response.customButton);
        } else {
          const source = {uri: response.uri};
          console.log('response', JSON.stringify(response));
          this.setState({
            filePath: response,
            fileData: response.data,
            fileUri: response.uri,
          });
        }
      } catch (error) {
        console.log(error);
      }
    });
  };

  return (
    <View>
      <Button title="take a photo" onPress={handleTaskPhoto}></Button>
      <Button title="open gallery" onPress={handleGallery}></Button>
    </View>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});

export default App;
